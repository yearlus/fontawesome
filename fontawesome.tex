% arara: indent: {overwrite: yes, silent: yes,  trace: yes}

%% Copyright 2013 Xavier Danaux (xdanaux@gmail.com).
%% Modified 2015 Stephen Yearl (stephen.yearl@gmail.com)
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License version 1.3c,
% available at http://www.latex-project.org/lppl/.


\documentclass{ltxdoc}
\GetFileInfo{\jobname.sty}
\def\fileversion{4.3.0}
\def\filedate{May 10, 2015}
\usepackage{lmodern}
\usepackage[numbered]{hypdoc}
\usepackage{hologo}
\usepackage{hyperref, xcolor}
\definecolor{grullo}{rgb}{0.66, 0.6, 0.53}				% greyish		fa 1.0
\definecolor{myrtle}{rgb}{0.13, 0.26, 0.12}				% dark green	fa 2.0
\definecolor{lava}{rgb}{0.81, 0.06, 0.13}				% redish		fa 3.0
\definecolor{myblue}{rgb}{0.22,0.45,0.70}				% light blue 	fa 3.1
\definecolor{goldenpoppy}{rgb}{0.99, 0.76, 0.0}			% golden yellow	fa 3.2
\definecolor{lavender(floral)}{rgb}{0.71, 0.49, 0.86} 	%lavender		fa 4.0
\definecolor{mangotango}{rgb}{1.0, 0.51, 0.26}			% orange-ish	fa 4.1
\definecolor{kellygreen}{rgb}{0.3, 0.73, 0.09} 			% light green 	fa 4.2
\definecolor{lightcarminepink}{rgb}{0.9, 0.4, 0.38} % salmon pink 	fa 4.3
\hypersetup{colorlinks=true, linkcolor=myblue, urlcolor=myblue, hyperindex}
\usepackage{longtable, booktabs}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{xparse, ifthen}
\usepackage{\jobname}
\EnableCrossrefs
\CodelineIndex
\RecordChanges

\begin{document}
** THIS IS BY NO MEANS TO BE CONSIDERED CANONICAL **
\title{The \textsf{\jobname} package\\High quality web icons}
\author{%
Stephen Yearl\thanks{E-mail: \href{mailto:stephen.yearl@gmail.com}{\tt stephen.yearl@gmail.com}} (interim update to 4.3.0)\\%
  Xavier Danaux\thanks{E-mail: \href{mailto:xdanaux@gmail.com}{\tt xdanaux@gmail.com}} (\hologo{LaTeX} code)\\%
  Dave Gandy (font and icons design)}
\date{Version \fileversion (\filedate)}
\maketitle

\begin{abstract}
The \textsf{\jobname} package grants access to 519 (as of 4.3.0) web-related icons provided by the included \emph{Font Awesome} OTF and TTF fonts. \emph{Font Awesome} was designed by Dave Gandy and released\footnote{See \url{http://fontawesome.github.com/Font-Awesome} for more details about the font itself} under the open SIL Open Font License\footnote{Available at \url{http://scripts.sil.org/OFL}.}.

This package requires the \textsf{fontspec} package and either the \hologo{Xe}\hologo{(La)TeX} or Lua\hologo{(La)TeX} engine to load the included otf font.
\end{abstract}

\changes{v4.3.0}  {2015/05/04}{Update to match Font Awesome version 4.3.0, with XX new icons.}
\changes{v3.1.1}  {2013/05/10}{Update to match Font Awesome version 3.1.1, with 53 new icons.}
\changes{v3.0.2-1}{2013/03/23}{Bigfix release: corrected the swap of the \cs{text-height} and \cs{text-width} icons.}
\changes{v3.0.2}  {2013/03/21}{First public release (version number set to match the included FontAwesome.otf font version).}
\makeatletter
\let\PrintMacroName@original\PrintMacroName
%\let\PrintDescribeMacro\@gobble
%\let\PrintDescribeEnv\@gobble
\let\PrintMacroName\@gobble
%\let\PrintEnvName\@gobble
\begin{macro}{\faTextHeight}
\changes{v3.0.2-1}{2013/03/23}{Corrected binding.}
\end{macro}
\begin{macro}{\faTextWidth}
\changes{v3.0.2-1}{2013/03/23}{Corrected binding.}
\end{macro}
\let\PrintMacroName\PrintMacroName@original
\makeatother

\bigskip

\section{Introduction}
This is a redistribution of the free (as in beer) \emph{Font Awesome} OTF font with specific bindings for \hologo{(La)TeX}. The \textsf{\jobname} package aims to enable easy access in \hologo{(La)TeX} to high quality icons covering:

\begin{multicols}{2}
\begin{itemize}
\itemsep -2pt % Reduce space between items
\item \hyperref[section:web_application]{Web Application Icons}
\item \hyperref[section:transportation]{Transportation Icons}
\item \hyperref[section:gender]{Gender Icons}
\item \hyperref[section:file_types]{File Type Icons}
\item \hyperref[section:spinner]{Spinner Icon}
\item \hyperref[section:form_control]{Form Control Icons}
\item \hyperref[section:payment]{Payment Icons}
\item \hyperref[section:chart]{Chart Icons}
\item \hyperref[section:currency]{Currency Icons}
\item \hyperref[section:text_editor]{Text Editor Icons}
\item \hyperref[section:directional]{Directional Icons}
\item \hyperref[section:currency]{Currency Icons}
\item \hyperref[section:video_player]{Video Player Icons},
\item \hyperref[section:brand]{Brand Icons}
\item \hyperref[section:medical]{Medical Icons}
\end{itemize}
\end{multicols}

\hyperref[section:centzon400]{Additions since 3.1.1},
\hyperref[section:hex]{arranged by hex value},


\section{Requirements}
The \textsf{\jobname} package requires the \textsf{fontspec} package and either the \hologo{Xe}\hologo{(La)TeX} or Lua\hologo{(La)TeX} engine to load the included OTF font.

\section{Usage}
\DescribeMacro{\faicon}
Once the \textsf{\jobname} package is loaded, icons can be accessed through the general \cs{faicon}, which takes as mandatory argument the \meta{name} of the desired icon, or through a direct command specific to each icon. The full list of icon designs, names and direct commands are showcased next.

\newenvironment{showcase}%
  {%
%   \begin{longtable}{ccp{3cm}p{3.5cm}p{1cm}}% debug: shows icons with both generic and specific commands

%\begin{longtable}{cp{3cm}p{3.5cm}p{1cm}p{1cm}}
   \begin{longtable}{cp{4cm}p{5cm}p{1.5cm}p{1.5cm}p{1.5cm}}
   \cmidrule[\heavyrulewidth]{1-5}% \toprule
%   \bfseries Icon& \bfseries Icon& \bfseries Name& \bfseries Direct command& \\% debug
   \bfseries Icon& \bfseries Name& \bfseries Direct command& \bfseries Hex Value& \bfseries Version\\
   \cmidrule{1-5}\endhead}
  {\cmidrule[\heavyrulewidth]{1-5}% \bottomrule
   \end{longtable}}
\NewDocumentCommand{\showcaseicon}{mmgg}{%
%  \faicon{#1}& \csname#2\endcsname& \itshape #1& \ttfamily \textbackslash #2\index{\ttfamily \textbackslash #2}& \IfNoValueTF{#3}{}{\tag{#3}}\\}% debug

% %ORIG
 % \faicon{#1}& \itshape #1& \ttfamily \textbackslash #2\index{\ttfamily \textbackslash #2}& \IfNoValueTF{#3}{}{\tag{#3}}\\}
  
  \faicon{#1}& \itshape #1& \ttfamily \textbackslash #2\index{\ttfamily \textbackslash #2}& \ttfamily #3& \IfNoValueTF{#4}{}{\tag{#4}}\\}
  
\newcommand{\tag}[1]{{%
  \small\sffamily%
      \ifthenelse{\equal{#1}{1.0}}{%
        \tikz[baseline={(TAG.base)}]{
          \node[white, fill=grullo, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 1.0\vphantom{Ay!}};
        }}{}%
      \ifthenelse{\equal{#1}{2.0}}{%
        \tikz[baseline={(TAG.base)}]{
          \node[white, fill=myrtle, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 2.0\vphantom{Ay!}};
        }}{}%
        \ifthenelse{\equal{#1}{3.0}}{%
          \tikz[baseline={(TAG.base)}]{
            \node[white, fill=lava, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 3.0\vphantom{Ay!}};
          }}{}%        
        \ifthenelse{\equal{#1}{3.1}}{%
          \tikz[baseline={(TAG.base)}]{
            \node[white, fill=myblue, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 3.1\vphantom{Ay!}};
          }}{}%
        \ifthenelse{\equal{#1}{3.2}}{%
	      \tikz[baseline={(TAG.base)}]{
	        \node[white, fill=goldenpoppy, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 3.2\vphantom{Ay!}};
	      }}{}%
		\ifthenelse{\equal{#1}{4.0}}{%
		  \tikz[baseline={(TAG.base)}]{
		   \node[white, fill=lavender(floral), rounded corners=3pt, inner sep=1.5pt] (TAG) {v 4.0\vphantom{Ay!}};
	    }}{}%
	    \ifthenelse{\equal{#1}{4.1}}{%
	      \tikz[baseline={(TAG.base)}]{
	        \node[white, fill=mangotango, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 4.1\vphantom{Ay!}};
        }}{}% 
	    \ifthenelse{\equal{#1}{4.2}}{%
	      \tikz[baseline={(TAG.base)}]{
	        \node[white, fill=kellygreen, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 4.2\vphantom{Ay!}};
        }}{}%      
	    \ifthenelse{\equal{#1}{4.3}}{%
	      \tikz[baseline={(TAG.base)}]{
	        \node[white, fill=lightcarminepink, rounded corners=3pt, inner sep=1.5pt] (TAG) {v 4.3\vphantom{Ay!}};
        }}{}%
  \ifthenelse{\equal{#1}{gone}}{%
    \tikz[baseline={(TAG.base)}]{
      \node[black!50, fill=black!25, rounded corners=3pt, inner sep=1.5pt] (TAG) {gone (?)\vphantom{Ay!}};
    }}{}%
  \ifthenelse{\equal{#1}{alias}}{%
    \textcolor{black!50}{(alias)}}{}%
  }}
  
  \subsection{Listing by Hexadecimal Value\label{section:hex}}
  \begin{showcase}
\showcaseicon{glass}{faGlass}{F000}{1.0}
\showcaseicon{music}{faMusic}{F001}{1.0} 
\showcaseicon{search}{faSearch}{F002}{1.0}
\showcaseicon{envelope}{faEnvelope}{F003}{1.0}
\showcaseicon{heart}{faHeart}{F004}{1.0}
\showcaseicon{star}{faStar}{F005}{1.0}
\showcaseicon{star-empty}{faStarEmpty}{F006}{1.0}
\showcaseicon{user}{faUser}{F007}{1.0}
\showcaseicon{film}{faFilm}{F008}{1.0}
\showcaseicon{thumbnails-large}{faThumbnailsLarge}{F009}{1.0}
\showcaseicon{thumbnails}{faThumbnails}{F00A}{1.0}
\showcaseicon{thumbnails-list}{faThumbnailsList}{F00B}{1.0}
\showcaseicon{ok}{faOk}{F00C}{1.0}
\showcaseicon{remove}{faRemove}{F00D}{1.0}
\showcaseicon{zoom-in}{faZoomIn}{F00E}{1.0}
\showcaseicon{zoom-out}{faZoomOut}{F010}{1.0}
\showcaseicon{off}{faOff}{F011}{1.0}
\showcaseicon{signal}{faSignal}{F012}{1.0}
\showcaseicon{cog}{faCog}{F013}{1.0}
\showcaseicon{trash}{faTrash}{F014}{1.0}
\showcaseicon{home}{faHome}{F015}{1.0}
\showcaseicon{file}{faFile}{F016}{1.0}
\showcaseicon{time}{faTime}{F017}{1.0}
\showcaseicon{road}{faRoad}{F018}{1.0}
\showcaseicon{download-alt}{faDownloadAlt}{F019}{1.0}
\showcaseicon{download}{faDownload}{F01A}{1.0}
\showcaseicon{upload}{faUpload}{F01B}{1.0}
\showcaseicon{inbox}{faInbox}{F01C}{1.0}
\showcaseicon{play-circle}{faPlayCircle}{F01D}{1.0}
\showcaseicon{repeat}{faRepeat}{F01E}{1.0}
\showcaseicon{refresh}{faRefresh}{F021}{1.0}
\showcaseicon{list-alt}{faListAlt}{F022}{1.0}
\showcaseicon{lock}{faLock}{F023}{1.0}
\showcaseicon{flag}{faFlag}{F024}{1.0}
\showcaseicon{headphones}{faHeadphones}{F025}{1.0}
\showcaseicon{volume-off}{faVolumeOff}{F026}{1.0}
\showcaseicon{volume-down}{faVolumeDown}{F027}{1.0}
\showcaseicon{volume-up}{faVolumeUp}{F028}{1.0}
\showcaseicon{qrcode}{faQRcode}{F029}{1.0}
\showcaseicon{barcode}{faBarcode}{F02A}{1.0}
\showcaseicon{tag}{faTag}{F02B}{1.0}
\showcaseicon{tags}{faTags}{F02C}{1.0}
\showcaseicon{book}{faBook}{F02D}{1.0}
\showcaseicon{bookmark}{faBookmark}{F02E}{1.0}
\showcaseicon{print}{faPrint}{F02F}{1.0}
\showcaseicon{camera}{faCamera}{F030}{1.0}
\showcaseicon{font}{faFont}{F031}{1.0}
\showcaseicon{bold}{faBold}{F032}{1.0}
\showcaseicon{italic}{faItalic}{F033}{1.0}
\showcaseicon{text-height}{faTextHeight}{F034}{1.0}
\showcaseicon{text-width}{faTextWidth}{F035}{1.0}
\showcaseicon{align-left}{faAlignLeft}{F036}{1.0}
\showcaseicon{align-center}{faAlignCenter}{F037}{1.0}
\showcaseicon{align-right}{faAlignRight}{F038}{1.0}
\showcaseicon{align-justify}{faAlignJustify}{F039}{1.0}
\showcaseicon{list}{faList}{F03A}{1.0}
\showcaseicon{indent-left}{faIndentLeft}{F03B}{1.0}
\showcaseicon{indent-right}{faIndentRight}{F03C}{1.0}
\showcaseicon{facetime-video}{faFacetimeVideo}{F03D}{1.0}
\showcaseicon{picture}{faPicture}{F03E}{1.0}
\showcaseicon{pencil}{faPencil}{F040}{1.0}
\showcaseicon{map-marker}{faMapMarker}{F041}{1.0}
\showcaseicon{adjust}{faAdjust}{F042}{1.0}
\showcaseicon{tint}{faTint}{F043}{1.0}
\showcaseicon{edit}{faEdit}{F044}{1.0}
\showcaseicon{share}{faShare}{F045}{1.0}
\showcaseicon{check}{faCheck}{F046}{1.0}
\showcaseicon{move}{faMove}{F047}{1.0}
\showcaseicon{step-backward}{faStepBackward}{F048}{1.0}
\showcaseicon{fast-backward}{faFastBackward}{F049}{1.0}
\showcaseicon{backward}{faBackward}{F04A}{1.0}
\showcaseicon{play}{faPlay}{F04B}{1.0}
\showcaseicon{pause}{faPause}{F04C}{1.0}
\showcaseicon{stop}{faStop}{F04D}{1.0}
\showcaseicon{forward}{faForward}{F04E}{1.0}
\showcaseicon{fast-forward}{faFastForward}{F050}{1.0}
\showcaseicon{step-forward}{faStepForward}{F051}{1.0}
\showcaseicon{eject}{faEject}{F052}{1.0}
\showcaseicon{chevron-left}{faChevronLeft}{F053}{1.0}
\showcaseicon{chevron-right}{faChevronRight}{F054}{1.0}
\showcaseicon{plus-sign}{faPlusSign}{F055}{1.0}
\showcaseicon{minus-sign}{faMinusSign}{F056}{1.0}
\showcaseicon{remove-sign}{faRemoveSign}{F057}{1.0}
\showcaseicon{ok-sign}{faOkSign}{F058}{1.0}
\showcaseicon{question-sign}{faQuestionSign}{F059}{1.0}
\showcaseicon{info-sign}{faInfoSign}{F05A}{1.0}
\showcaseicon{screenshot}{faScreenshot}{F05B}{1.0}
\showcaseicon{remove-circle}{faRemoveCircle}{F05C}{1.0}
\showcaseicon{ok-circle}{faOkCircle}{F05D}{1.0}
\showcaseicon{ban-circle}{faBanCircle}{F05E}{1.0}
\showcaseicon{arrow-left}{faArrowLeft}{F060}{1.0}
\showcaseicon{arrow-right}{faArrowRight}{F061}{1.0}
\showcaseicon{arrow-up}{faArrowUp}{F062}{1.0}
\showcaseicon{arrow-down}{faArrowDown}{F063}{1.0}
\showcaseicon{share-alt}{faShareAlt}{F064}{1.0}
\showcaseicon{resize-full}{faResizeFull}{F065}{1.0}
\showcaseicon{resize-small}{faResizeSmall}{F066}{1.0}
\showcaseicon{plus}{faPlus}{F067}{1.0}
\showcaseicon{minus}{faMinus}{F068}{1.0}
\showcaseicon{asterisk}{faAsterisk}{F069}{1.0}
\showcaseicon{exclamation-sign}{faExclamationSign}{F06A}{1.0}
\showcaseicon{gift}{faGift}{F06B}{1.0}
\showcaseicon{leaf}{faLeaf}{F06C}{1.0}
\showcaseicon{fire}{faFire}{F06D}{1.0}
\showcaseicon{eye-open}{faEyeOpen}{F06E}{1.0}
\showcaseicon{eye-close}{faEyeClose}{F070}{1.0}
\showcaseicon{warning-sign}{faWarningSign}{F071}{1.0}
\showcaseicon{plane}{faPlane}{F072}{1.0}
\showcaseicon{calendar}{faCalendar}{F073}{1.0}
\showcaseicon{random}{faRandom}{F074}{1.0}
\showcaseicon{comment}{faComment}{F075}{1.0}
\showcaseicon{magnet}{faMagnet}{F076}{1.0}
\showcaseicon{chevron-up}{faChevronUp}{F077}{1.0}
\showcaseicon{chevron-down}{faChevronDown}{F078}{1.0}
\showcaseicon{retweet}{faRetweet}{F079}{1.0}
\showcaseicon{shopping-cart}{faShoppingCart}{F07A}{1.0}
\showcaseicon{folder-close}{faFolderClose}{F07B}{1.0}
\showcaseicon{folder-open}{faFolderOpen}{F07C}{1.0}
\showcaseicon{resize-vertical}{faResizeVertical}{F07D}{1.0}
\showcaseicon{resize-horizontal}{faResizeHorizontal}{F07E}{1.0}
\showcaseicon{bar-chart}{faBarChart}{F080}{1.0}
\showcaseicon{twitter-sign}{faTwitterSign}{F081}{2.0}
\showcaseicon{facebook-sign}{faFacebookSign}{F082}{2.0}
\showcaseicon{camera-retro}{faCameraRetro}{F083}{2.0}
\showcaseicon{key}{faKey}{F084}{2.0}
\showcaseicon{cogs}{faCogs}{F085}{2.0}
\showcaseicon{comments}{faComments}{F086}{2.0}
\showcaseicon{thumbs-up}{faThumbsUp}{F087}{2.0}
\showcaseicon{thumbs-down}{faThumbsDown}{F088}{2.0}
\showcaseicon{star-half}{faStarHalf}{F089}{2.0}
\showcaseicon{heart-empty}{faHeartEmpty}{F08A}{2.0}
\showcaseicon{signout}{faSignout}{F08B}{2.0}
\showcaseicon{linkedin-sign}{faLinkedinSign}{F08C}{2.0}
\showcaseicon{pushpin}{faPushpin}{F08D}{2.0}
\showcaseicon{external-link}{faExternalLink}{F08E}{2.0}
\showcaseicon{signin}{faSignin}{F090}{2.0}
\showcaseicon{trophy}{faTrophy}{F091}{2.0}
\showcaseicon{github-sign}{faGithubSign}{F092}{2.0}
\showcaseicon{upload-alt}{faUploadAlt}{F093}{2.0}
\showcaseicon{lemon}{faLemon}{F094}{2.0}
\showcaseicon{phone}{faPhone}{F095}{2.0}
\showcaseicon{check-empty}{faCheckEmpty}{F096}{2.0}
\showcaseicon{bookmark-empty}{faBookmarkEmpty}{F097}{2.0}
\showcaseicon{phone-sign}{faPhoneSign}{F098}{2.0}
\showcaseicon{twitter}{faTwitter}{F099}{2.0}
\showcaseicon{facebook}{faFacebook}{F09A}{2.0}
\showcaseicon{github}{faGithub}{F09B}{2.0}
\showcaseicon{unlock}{faUnlock}{F09C}{2.0}
\showcaseicon{credit-card}{faCreditCard}{F09D}{2.0}
\showcaseicon{rss}{faRSS}{F09E}{2.0}
\showcaseicon{hdd}{faHDD}{F0A0}{2.0}
\showcaseicon{bullhorn}{faBullhorn}{F0A1}{2.0}
\showcaseicon{bell}{faBell}{F0A2}{2.0}
\showcaseicon{certificate}{faCertificate}{F0A3}{2.0}
\showcaseicon{hand-left}{faHandLeft}{F0A4}{2.0}
\showcaseicon{hand-right}{faHandRight}{F0A5}{2.0}
\showcaseicon{hand-up}{faHandUp}{F0A6}{2.0}
\showcaseicon{hand-down}{faHandDown}{F0A7}{2.0}
\showcaseicon{circle-arrow-left}{faCircleArrowLeft}{F0A8}{2.0}
\showcaseicon{circle-arrow-right}{faCircleArrowRight}{F0A9}{2.0}
\showcaseicon{circle-arrow-up}{faCircleArrowUp}{F0AA}{2.0}
\showcaseicon{circle-arrow-down}{faCircleArrowDown}{F0AB}{2.0}
\showcaseicon{globe}{faGlobe}{F0AC}{2.0}
\showcaseicon{wrench}{faWrench}{F0AD}{2.0}
\showcaseicon{tasks}{faTasks}{F0AE}{2.0}
\showcaseicon{filter}{faFilter}{F0B0}{2.0}
\showcaseicon{briefcase}{faBriefcase}{F0B1}{2.0}
\showcaseicon{fullscreen}{faFullscreen}{F0B2}{2.0}
\showcaseicon{group}{faGroup}{F0C0}{2.0}
\showcaseicon{link}{faLink}{F0C1}{2.0}
\showcaseicon{cloud}{faCloud}{F0C2}{2.0}
\showcaseicon{beaker}{faBeaker}{F0C3}{2.0}
\showcaseicon{cut}{faCut}{F0C4}{2.0}
\showcaseicon{copy}{faCopy}{F0C5}{2.0}
\showcaseicon{paper-clip}{faPaperClip}{F0C6}{2.0}
\showcaseicon{save}{faSave}{F0C7}{2.0}
\showcaseicon{sign-blank}{faSignBlank}{F0C8}{2.0}
\showcaseicon{reorder}{faReorder}{F0C9}{2.0}
\showcaseicon{list-ul}{faListUL}{F0CA}{2.0}
\showcaseicon{list-ol}{faListOL}{F0CB}{2.0}
\showcaseicon{strikethrough}{faStrikethrough}{F0CC}{2.0}
\showcaseicon{underline}{faUnderline}{F0CD}{2.0}
\showcaseicon{table}{faTable}{F0CE}{2.0}
\showcaseicon{magic}{faMagic}{F0D0}{2.0}
\showcaseicon{truck}{faTruck}{F0D1}{2.0}
\showcaseicon{pinterest}{faPinterest}{F0D2}{2.0}
\showcaseicon{pinterest-sign}{faPinterestSign}{F0D3}{2.0}
\showcaseicon{google-plus-sign}{faGooglePlusSign}{F0D4}{2.0}
\showcaseicon{google-plus}{faGooglePlus}{F0D5}{2.0}
\showcaseicon{money}{faMoney}{F0D6}{2.0}
\showcaseicon{caret-down}{faCaretDown}{F0D7}{2.0}
\showcaseicon{caret-up}{faCaretUp}{F0D8}{2.0}
\showcaseicon{caret-left}{faCaretLeft}{F0D9}{2.0}
\showcaseicon{caret-right}{faCaretRight}{F0DA}{2.0}
\showcaseicon{columns}{faColumns}{F0DB}{2.0}
\showcaseicon{sort}{faSort}{F0DC}{2.0}
\showcaseicon{sort-down}{faSortDown}{F0DD}{2.0}
\showcaseicon{sort-up}{faSortUp}{F0DE}{2.0}
\showcaseicon{envelope-alt}{faEnvelopeAlt}{F0E0}{2.0}
\showcaseicon{linkedin}{faLinkedin}{F0E1}{2.0}
\showcaseicon{undo}{faUndo}{F0E2}{2.0}
\showcaseicon{legal}{faLegal}{F0E3}{2.0}
\showcaseicon{dashboard}{faDashboard}{F0E4}{2.0}
\showcaseicon{comment-alt}{faCommentAlt}{F0E5}{2.0}
\showcaseicon{comments-alt}{faCommentsAlt}{F0E6}{2.0}
\showcaseicon{bolt}{faBolt}{F0E7}{2.0}
\showcaseicon{sitemap}{faSitemap}{F0E8}{2.0}
\showcaseicon{umbrella}{faUmbrella}{F0E9}{2.0}
\showcaseicon{paste}{faPaste}{F0EA}{2.0}
\showcaseicon{lightbulb}{faLightbulb}{F0EB}{2.0}
\showcaseicon{exchange}{faExchange}{F0EC}{2.0}
\showcaseicon{cloud-download}{faCloudDownload}{F0ED}{2.0}
\showcaseicon{cloud-upload}{faCloudUpload}{F0EE}{2.0}
\showcaseicon{user-md}{faUserMD}{F0F0}{2.0}
\showcaseicon{stethoscope}{faStethoscope}{F0F1}{3.0}
\showcaseicon{suitcase}{faSuitcase}{F0F2}{3.0}
\showcaseicon{bell-alt}{faBellAlt}{F0F3}{3.0}
\showcaseicon{coffee}{faCoffee}{F0F4}{3.0}
\showcaseicon{food}{faFood}{F0F5}{3.0}
\showcaseicon{file-alt}{faFileAlt}{F0F6}{3.0}
\showcaseicon{building}{faBuilding}{F0F7}{3.0}
\showcaseicon{hospital}{faHospital}{F0F8}{3.0}
\showcaseicon{ambulance}{faAmbulance}{F0F9}{3.0}
\showcaseicon{medical-kit}{faMedicalKit}{F0FA}{3.0}
\showcaseicon{fighter-jet}{faFighterJet}{F0FB}{3.0}
\showcaseicon{beer}{faBeer}{F0FC}{3.0}
\showcaseicon{hospital-sign}{faHospitalSign}{F0FD}{3.0}
\showcaseicon{medical-sign}{faMedicalSign}{F0FE}{3.0}
\showcaseicon{double-angle-left}{faDoubleAngleLeft}{F100}{3.0}
\showcaseicon{double-angle-right}{faDoubleAngleRight}{F101}{3.0}
\showcaseicon{double-angle-up}{faDoubleAngleUp}{F102}{3.0}
\showcaseicon{double-angle-down}{faDoubleAngleDown}{F103}{3.0}
\showcaseicon{angle-left}{faAngleLeft}{F104}{3.0}
\showcaseicon{angle-right}{faAngleRight}{F105}{3.0}
\showcaseicon{angle-up}{faAngleUp}{F106}{3.0}
\showcaseicon{angle-down}{faAngleDown}{F107}{3.0}
\showcaseicon{desktop}{faDesktop}{F108}{3.0}
\showcaseicon{laptop}{faLaptop}{F109}{3.0}
\showcaseicon{tablet}{faTablet}{F10A}{3.0}
\showcaseicon{mobile-phone}{faMobilePhone}{F10B}{3.0}
\showcaseicon{circle-blank}{faCircleBlank}{F10C}{3.0}
\showcaseicon{quote-left}{faQuoteLeft}{F10D}{3.0}
\showcaseicon{quote-right}{faQuoteRight}{F10E}{3.0}
\showcaseicon{spinner}{faSpinner}{F110}{3.0}
\showcaseicon{circle}{faCircle}{F111}{3.0}
\showcaseicon{reply}{faReply}{F112}{3.0}
\showcaseicon{github-alt}{faGithubAlt}{F113}{3.0}
\showcaseicon{folder-open-alt}{faFolderOpenAlt}{F114}{3.0}
\showcaseicon{folder-close-alt}{faFolderCloseAlt}{F115}{3.0}
\showcaseicon{expand-alt}{faExpandAlt}{F116}{3.0}
\showcaseicon{collapse-alt}{faCollapseAlt}{F117}{3.0}
\showcaseicon{smile}{faSmile}{F118}{3.0}
\showcaseicon{frown}{faFrown}{F119}{3.0}
\showcaseicon{meh}{faMeh}{F11A}{3.0}
\showcaseicon{gamepad}{faGamepad}{F11B}{3.0}
\showcaseicon{keyboard}{faKeyboard}{F11C}{3.0}
\showcaseicon{flag-alt}{faFlagAlt}{F11D}{3.0}
\showcaseicon{flag-checkered}{faFlagCheckered}{F11E}{3.0}
\showcaseicon{terminal}{faTerminal}{F120}{3.1}
\showcaseicon{code}{faCode}{F121}{3.1}
\showcaseicon{reply-all}{faReplyAll}{F122}{3.1}
\showcaseicon{star-half-empty}{faStarHalfEmpty}{F123}{3.1}
\showcaseicon{location-arrow}{faLocationArrow}{F124}{3.1}
\showcaseicon{crop}{faCrop}{F125}{3.1}
\showcaseicon{code-fork}{faCodeFork}{F126}{3.1}
\showcaseicon{unlink}{faUnlink}{F127}{3.1}
\showcaseicon{question}{faQuestion}{F128}{3.1}
\showcaseicon{info}{faInfo}{F129}{3.1}
\showcaseicon{exclamation}{faExclamation}{F12A}{3.1}
\showcaseicon{superscript}{faSuperscript}{F12B}{3.1}
\showcaseicon{subscript}{faSubscript}{F12C}{3.1}
\showcaseicon{eraser}{faEraser}{F12D}{3.1}
\showcaseicon{puzzle-piece}{faPuzzlePiece}{F12E}{3.1}
\showcaseicon{microphone}{faMicrophone}{F130}{3.1}
\showcaseicon{microphone-off}{faMicrophoneOff}{F131}{3.1}
\showcaseicon{shield}{faShield}{F132}{3.1}
\showcaseicon{calendar-empty}{faCalendarEmpty}{F133}{3.1}
\showcaseicon{fire-extinguisher}{faFireExtinguisher}{F134}{3.1}
\showcaseicon{rocket}{faRocket}{F135}{3.1}
\showcaseicon{maxcdn}{faMaxCDN}{F136}{3.1}
\showcaseicon{chevron-sign-left}{faChevronSignLeft}{F137}{3.1}
\showcaseicon{chevron-sign-right}{faChevronSignRight}{F138}{3.1}
\showcaseicon{chevron-sign-up}{faChevronSignUp}{F139}{3.1}
\showcaseicon{chevron-sign-down}{faChevronSignDown}{F13A}{3.1}
\showcaseicon{html5}{faHTMLfive}{F13B}{3.1}
\showcaseicon{css3}{faCSSthree}{F13C}{3.1}
\showcaseicon{anchor}{faAnchor}{F13D}{3.1}
\showcaseicon{unlock-alt}{faUnlockAlt}{F13E}{3.1}
\showcaseicon{bullseye}{faBullseye}{F140}{3.1}
\showcaseicon{ellipsis-horizontal}{faEllipsisHorizontal}{F141}{3.1}
\showcaseicon{ellipsis-vertical}{faEllipsisVertical}{F142}{3.1}
\showcaseicon{rss-sign}{faRSSSign}{F143}{3.1}
\showcaseicon{play-sign}{faPlaySign}{F144}{3.1}
\showcaseicon{ticket}{faTicket}{F145}{3.1}
\showcaseicon{minus-sign-alt}{faMinusSignAlt}{F146}{3.1}
\showcaseicon{check-minus}{faCheckMinus}{F147}{3.1}
\showcaseicon{level-up}{faLevelUp}{F148}{3.1}
\showcaseicon{level-down}{faLevelDown}{F149}{3.1}
\showcaseicon{check-sign}{faCheckSign}{F14A}{3.1}
\showcaseicon{edit-sign}{faEditSign}{F14B}{3.1}
\showcaseicon{external-link-sign}{faExternalLinkSign}{F14C}{3.1}
\showcaseicon{share-sign}{faShareSign}{F14D}{3.1}


%%%%%%%%%%%%%%%%%%%%%%%%centzon400 add 2015-05-04

\showcaseicon{compass}{faCompass}{F14E}{3.2}
\showcaseicon{fa-caret-square-o-down}{fa-caret-square-o-down}{F150}{3.2}
\showcaseicon{fa-caret-square-o-up}{fa-caret-square-o-up}{F151}{3.2}
\showcaseicon{fa-caret-square-o-right}{fa-caret-square-o-right}{F152}{3.2}
\showcaseicon{fa-eur}{fa-eur}{F153}{3.2}
\showcaseicon{fa-euro}{fa-euro}{F153}{3.2}
\showcaseicon{fa-gbp}{fa-gbp}{F154}{3.2}
\showcaseicon{fa-dollar}{fa-dollar}{F155}{3.2}
\showcaseicon{fa-usd}{fa-usd}{F155}{3.2}
\showcaseicon{fa-rupee}{fa-rupee}{F156}{3.2}
\showcaseicon{fa-inr}{fa-inr}{F156}{3.2}
\showcaseicon{fa-cny}{fa-cny}{F157}{3.2}
\showcaseicon{fa-rmb}{fa-rmb}{F157}{3.2}
\showcaseicon{fa-yen}{fa-yen}{F157}{3.2}
\showcaseicon{fa-jpy}{fa-jpy}{F157}{3.2}
\showcaseicon{fa-ruble}{fa-ruble}{F158}{3.2}
\showcaseicon{fa-rouble}{fa-rouble}{F158}{3.2}
\showcaseicon{fa-rub}{fa-rub}{F158}{3.2}
\showcaseicon{fa-won}{fa-won}{F159}{3.2}
\showcaseicon{fa-krw}{fa-krw}{F159}{3.2}
\showcaseicon{fa-bitcoin}{fa-bitcoin}{F15A}{3.2}
\showcaseicon{fa-btc}{fa-btc}{F15A}{3.2}
\showcaseicon{fa-file}{fa-file}{F15B}{3.2}
\showcaseicon{fa-file-text}{fa-file-text}{F15C}{3.2}
\showcaseicon{fa-sort-alpha-asc}{fa-sort-alpha-asc}{F15D}{3.2}
\showcaseicon{fa-sort-alpha-desc}{fa-sort-alpha-desc}{F15E}{3.2}
\showcaseicon{fa-sort-amount-asc}{fa-sort-amount-asc}{F160}{3.2}
\showcaseicon{fa-sort-amount-desc}{fa-sort-amount-desc}{F161}{3.2}
\showcaseicon{fa-sort-numeric-asc}{fa-sort-numeric-asc}{F162}{3.2}
\showcaseicon{fa-sort-numeric-desc}{fa-sort-numeric-desc}{F163}{3.2}
\showcaseicon{fa-thumbs-up}{fa-thumbs-up}{F164}{3.2}
\showcaseicon{fa-thumbs-down}{fa-thumbs-down}{F165}{3.2}
\showcaseicon{fa-youtube-square}{fa-youtube-square}{F166}{3.2}
\showcaseicon{fa-youtube}{fa-youtube}{F167}{3.2}
\showcaseicon{fa-xing}{fa-xing}{F168}{3.2}
\showcaseicon{fa-xing-square}{fa-xing-square}{F169}{3.2}
\showcaseicon{fa-youtube-play}{fa-youtube-play}{F16A}{3.2}
\showcaseicon{fa-dropbox}{fa-dropbox}{F16B}{3.2}
\showcaseicon{fa-stack-overflow}{fa-stack-overflow}{F16C}{3.2}
\showcaseicon{fa-instagram}{fa-instagram}{F16D}{3.2}
\showcaseicon{fa-flickr}{fa-flickr}{F16E}{3.2}
\showcaseicon{fa-adn}{fa-adn}{F170}{3.2}
\showcaseicon{fa-bitbucket}{fa-bitbucket}{F171}{3.2}
\showcaseicon{fa-bitbucket-square}{fa-bitbucket-square}{F172}{3.2}
\showcaseicon{fa-tumblr}{fa-tumblr}{F173}{3.2}
\showcaseicon{fa-tumblr-square}{fa-tumblr-square}{F174}{3.2}
\showcaseicon{fa-long-arrow-down}{fa-long-arrow-down}{F175}{3.2}
\showcaseicon{fa-long-arrow-up}{fa-long-arrow-up}{F176}{3.2}
\showcaseicon{fa-long-arrow-left}{fa-long-arrow-left}{F177}{3.2}
\showcaseicon{fa-long-arrow-right}{fa-long-arrow-right}{F178}{3.2}
\showcaseicon{fa-apple}{fa-apple}{F179}{3.2}
\showcaseicon{fa-windows}{fa-windows}{F17A}{3.2}
\showcaseicon{fa-android}{fa-android}{F17B}{3.2}
\showcaseicon{fa-linux}{fa-linux}{F17C}{3.2}
\showcaseicon{fa-dribbble}{fa-dribbble}{F17D}{3.2}
\showcaseicon{fa-skype}{fa-skype}{F17E}{3.2}
\showcaseicon{fa-foursquare}{fa-foursquare}{F180}{3.2}
\showcaseicon{fa-trello}{fa-trello}{F181}{3.2}
\showcaseicon{fa-female}{fa-female}{F182}{3.2}
\showcaseicon{fa-male}{fa-male}{F183}{3.2}
\showcaseicon{fa-gittip}{fa-gittip}{F184}{3.2}
\showcaseicon{fa-gratipay}{fa-gratipay}{F184}{3.2}
\showcaseicon{fa-sun-o}{fa-sun-o}{F185}{3.2}
\showcaseicon{fa-moon-o}{fa-moon-o}{F186}{3.2}
\showcaseicon{fa-archive}{fa-archive}{F187}{3.2}
\showcaseicon{fa-bug}{fa-bug}{F188}{3.2}
\showcaseicon{fa-vk}{fa-vk}{F189}{3.2}
\showcaseicon{fa-weibo}{fa-weibo}{F18A}{3.2}
\showcaseicon{fa-renren}{fa-renren}{F18B}{3.2}
\showcaseicon{fa-pagelines}{fa-pagelines}{F18C}{4.0}
\showcaseicon{fa-stack-exchange}{fa-stack-exchange}{F18D}{4.0}
\showcaseicon{fa-arrow-circle-o-right}{fa-arrow-circle-o-right}{F18E}{4.0}
\showcaseicon{fa-arrow-circle-o-left}{fa-arrow-circle-o-left}{F190}{4.0}
\showcaseicon{fa-toggle-left}{fa-toggle-left}{F191}{4.0}
\showcaseicon{fa-caret-square-o-left}{fa-caret-square-o-left}{F191}{4.0}
\showcaseicon{fa-dot-circle-o}{fa-dot-circle-o}{F192}{4.0}
\showcaseicon{fa-wheelchair}{fa-wheelchair}{F193}{4.0}
\showcaseicon{fa-vimeo-square}{fa-vimeo-square}{F194}{4.0}
\showcaseicon{fa-turkish-lira}{fa-turkish-lira}{F195}{4.0}
\showcaseicon{fa-try}{fa-try}{F195}{4.0}
\showcaseicon{fa-plus-square-o}{fa-plus-square-o}{F196}{4.0}
\showcaseicon{fa-space-shuttle}{fa-space-shuttle}{F197}{4.1}
\showcaseicon{fa-slack}{fa-slack}{F198}{4.1}
\showcaseicon{fa-envelope-square}{fa-envelope-square}{F199}{4.1}
\showcaseicon{fa-wordpress}{fa-wordpress}{F19A}{4.1}
\showcaseicon{wordpress}{faWordpress}{F19A}{4.1}
\showcaseicon{fa-openid}{fa-openid}{F19B}{4.1}
\showcaseicon{fa-institution}{fa-institution}{F19C}{4.1}
\showcaseicon{fa-bank}{fa-bank}{F19C}{4.1}
\showcaseicon{fa-university}{fa-university}{F19C}{4.1}
\showcaseicon{fa-mortar-board}{fa-mortar-board}{F19D}{4.1}
\showcaseicon{fa-graduation-cap}{fa-mortar-board}{F19D}{4.1}
\showcaseicon{fa-yahoo}{fa-yahoo}{F19E}{4.1}
\showcaseicon{fa-google}{fa-google}{F1A0}{4.1}
\showcaseicon{fa-reddit}{fa-reddit}{F1A1}{4.1}
\showcaseicon{fa-reddit-square}{fa-reddit-square}{F1A2}{4.1}
\showcaseicon{fa-stumbleupon-circle}{fa-stumbleupon-circle}{F1A3}{4.1}
\showcaseicon{fa-stumbleupon}{fa-stumbleupon}{F1A4}{4.1}
\showcaseicon{fa-delicious}{fa-delicious}{F1A5}{4.1}
\showcaseicon{fa-digg}{fa-digg}{F1A6}{4.1}
\showcaseicon{fa-pied-piper}{fa-pied-piper}{F1A7}{4.1}
\showcaseicon{fa-pied-piper-alt}{fa-pied-piper-alt}{F1A8}{4.1}
\showcaseicon{fa-drupal}{fa-drupal}{F1A9}{4.1}
\showcaseicon{fa-joomla}{fa-joomla}{F1AA}{4.1}
\showcaseicon{fa-language}{fa-language}{F1AB}{4.1}
\showcaseicon{fa-fax}{fa-fax}{F1AC}{4.1}
\showcaseicon{fa-building}{fa-building}{F1AD}{4.1}
\showcaseicon{fa-child}{fa-child}{F1AE}{4.1}
\showcaseicon{fa-paw}{fa-paw}{F1B0}{4.1}
\showcaseicon{fa-spoon}{fa-spoon}{F1B1}{4.1}
\showcaseicon{fa-cube}{fa-cube}{F1B2}{4.1}
\showcaseicon{fa-cubes}{fa-cubes}{F1B3}{4.1}
\showcaseicon{fa-behance}{fa-behance}{F1B4}{4.1}
\showcaseicon{fa-behance-square}{fa-behance-square}{F1B5}{4.1}
\showcaseicon{fa-steam}{fa-steam}{F1B6}{4.1}
\showcaseicon{fa-steam-square}{fa-steam-square}{F1B7}{4.1}
\showcaseicon{fa-recycle}{fa-recycle}{F1B8}{4.1}
\showcaseicon{fa-automobile}{fa-automobile}{F1B9}{4.1}
\showcaseicon{fa-car}{fa-car}{F1B9}{4.1}
\showcaseicon{fa-cab}{fa-cab}{F1BA}{4.1}
\showcaseicon{fa-taxi}{fa-taxi}{F1BA}{4.1}
\showcaseicon{fa-tree}{fa-tree}{F1BB}{4.1}
\showcaseicon{fa-spotify}{fa-spotify}{F1BC}{4.1}
\showcaseicon{fa-deviantart}{fa-deviantart}{F1BD}{4.1}
\showcaseicon{fa-soundcloud}{fa-soundcloud}{F1BE}{4.1}
\showcaseicon{fa-database}{fa-database}{F1C0}{4.1}
\showcaseicon{fa-file-pdf-o}{fa-file-pdf-o}{F1C1}{4.1}
\showcaseicon{fa-file-word-o}{fa-file-word-o}{F1C2}{4.1}
\showcaseicon{fa-file-excel-o}{fa-file-excel-o}{F1C3}{4.1}
\showcaseicon{fa-file-powerpoint-o}{fa-file-powerpoint-o}{F1C4}{4.1}
\showcaseicon{fa-file-photo-o}{fa-file-photo-o}{F1C5}{4.1}
\showcaseicon{fa-file-picture-o}{fa-file-picture-o}{F1C5}{4.1}
\showcaseicon{fa-file-image-o}{fa-file-image-o}{F1C5}{4.1}
\showcaseicon{fa-file-zip-o}{fa-file-zip-o}{F1C6}{4.1}
\showcaseicon{fa-file-archive-o}{fa-file-archive-o}{F1C6}{4.1}
\showcaseicon{fa-file-sound-o}{fa-file-sound-o}{F1C7}{4.1}
\showcaseicon{fa-file-audio-o}{fa-file-audio-o}{F1C7}{4.1}
\showcaseicon{fa-file-movie-o}{fa-file-movie-o}{F1C8}{4.1}
\showcaseicon{fa-file-video-o}{fa-file-video-o}{F1C8}{4.1}
\showcaseicon{fa-file-code-o}{fa-file-code-o}{F1C9}{4.1}
\showcaseicon{fa-vine}{fa-vine}{F1CA}{4.1}
\showcaseicon{fa-codepen}{fa-codepen}{F1CB}{4.1}
\showcaseicon{fa-jsfiddle}{fa-jsfiddle}{F1CC}{4.1}
\showcaseicon{fa-life-bouy}{fa-life-bouy}{F1CD}{4.1}
\showcaseicon{fa-life-buoy}{fa-life-buoy}{F1CD}{4.1}
\showcaseicon{fa-life-saver}{fa-life-saver}{F1CD}{4.1}
\showcaseicon{fa-support}{fa-support}{F1CD}{4.1}
\showcaseicon{fa-life-ring}{fa-life-ring}{F1CD}{4.1}
\showcaseicon{fa-circle-o-notch}{fa-circle-o-notch}{F1CE}{4.1}
\showcaseicon{fa-ra}{fa-ra}{F1D0}{4.1}
\showcaseicon{fa-rebel}{fa-rebel}{F1D0}{4.1}
\showcaseicon{fa-ge}{fa-ge}{F1D1}{4.1}
\showcaseicon{fa-empire}{fa-empire}{F1D1}{4.1}
\showcaseicon{fa-git-square}{fa-git-square}{F1D2}{4.1}
\showcaseicon{fa-git}{fa-git}{F1D3}{4.1}
\showcaseicon{fa-hacker-news}{fa-hacker-news}{F1D4}{4.1}
\showcaseicon{fa-tencent-weibo}{fa-tencent-weibo}{F1D5}{4.1}
\showcaseicon{fa-qq}{fa-qq}{F1D6}{4.1}
\showcaseicon{fa-wechat}{fa-wechat}{F1D7}{4.1}
\showcaseicon{fa-weixin}{fa-weixin}{F1D7}{4.1}
\showcaseicon{fa-send}{send}{F1D8}{4.1}
\showcaseicon{fa-paper-plane}{fa-paper-plane}{F1D8}{4.1}
\showcaseicon{fa-send-o}{fa-send-o}{F1D9}{4.1}
\showcaseicon{fa-history}{fa-history}{F1DA}{4.1}
\showcaseicon{fa-genderless}{fa-genderless}{F1DB}{4.1}
\showcaseicon{fa-circle-thin}{fa-circle-thin}{F1DB}{4.1}
\showcaseicon{fa-header}{fa-header}{F1DC}{4.1}
\showcaseicon{fa-paragraph}{fa-paragraph}{F1DD}{4.1}
\showcaseicon{fa-sliders}{fa-sliders}{F1DE}{4.1}
\showcaseicon{fa-share-alt}{fa-share-alt}{F1E0}{4.1}
\showcaseicon{fa-share-alt-square}{fa-share-alt-square}{F1E1}{4.1}
\showcaseicon{fa-bomb}{fa-bomb}{F1E2}{4.1}
\showcaseicon{fa-soccer-o}{fa-soccer-o}{F1E3}{4.2}
\showcaseicon{fa-futbol-o}{fa-futbol-o}{F1E3}{4.2}
\showcaseicon{fa-tty}{fa-tty}{F1E4}{4.2}
\showcaseicon{fa-binoculars}{fa-binoculars}{F1E5}{4.2}
\showcaseicon{fa-plug}{fa-plug}{F1E6}{4.2}
\showcaseicon{fa-slideshare}{fa-slideshare}{F1E7}{4.2}
\showcaseicon{fa-twitch}{fa-twitch}{F1E8}{4.2}
\showcaseicon{fa-yelp}{fa-yelp}{F1E9}{4.2}
\showcaseicon{fa-newspaper-o}{fa-newspaper-o}{F1EA}{4.2}
\showcaseicon{fa-wifi}{fa-wifi}{F1EB}{4.2}
\showcaseicon{fa-calculator}{fa-calculator}{F1EC}{4.2}
\showcaseicon{fa-paypal}{fa-paypal}{F1ED}{4.2}
\showcaseicon{fa-google-wallet}{fa-google-wallet}{F1EE}{4.2}
\showcaseicon{fa-cc-visa}{fa-cc-visa}{F1F0}{4.2}
\showcaseicon{fa-cc-mastercard}{fa-cc-mastercard}{F1F1}{4.2}
\showcaseicon{fa-cc-discover}{fa-cc-discover}{F1F2}{4.2}
\showcaseicon{fa-cc-amex}{fa-cc-amex}{F1F3}{4.2}
\showcaseicon{fa-cc-paypal}{fa-cc-paypal}{F1F4}{4.2}
\showcaseicon{fa-cc-stripe}{fa-cc-stripe}{F1F5}{4.2}
\showcaseicon{fa-bell-slash}{fa-bell-slash}{F1F6}{4.2}
\showcaseicon{fa-bell-slash-o}{fa-bell-slash-o}{F1F7}{4.2}
\showcaseicon{fa-trash}{fa-trash}{F1F8}{4.2}
\showcaseicon{fa-copyright}{fa-copyright}{F1F9}{4.2}
\showcaseicon{fa-at}{fa-at}{F1FA}{4.2}
\showcaseicon{fa-eyedropper}{fa-eyedropper}{F1FB}{4.2}
\showcaseicon{fa-paint-brush}{fa-paint-brush}{F1FC}{4.2}
\showcaseicon{fa-birthday-cake}{fa-birthday-cake}{F1FD}{4.2}
\showcaseicon{fa-area-chart}{fa-area-chart}{F1FE}{4.2}
\showcaseicon{fa-pie-chart}{fa-pie-chart}{F200}{4.2}
\showcaseicon{fa-line-chart}{fa-line-chart}{F201}{4.2}
\showcaseicon{fa-lastfm}{fa-lastfm}{F202}{4.2}
\showcaseicon{fa-lastfm-square}{fa-lastfm-square}{F203}{4.2}
\showcaseicon{fa-toggle-off}{fa-toggle-off}{F204}{4.2}
\showcaseicon{fa-toggle-on}{fa-toggle-on}{F205}{4.2}
\showcaseicon{fa-bicycle}{fa-bicycle}{F206}{4.2}
\showcaseicon{fa-bus}{fa-bus}{F207}{4.2}
\showcaseicon{fa-ioxhost}{fa-ioxhost}{F208}{4.2}
\showcaseicon{fa-angellist}{fa-angellist}{F209}{4.2}
\showcaseicon{fa-cc}{fa-cc}{F20A}{4.2}
\showcaseicon{fa-shekel}{fa-shekel}{F20B}{4.2}
\showcaseicon{fa-sheqel}{fa-sheqel}{F20B}{4.2}
\showcaseicon{fa-ils}{fa-ils}{F20B}{4.2}
\showcaseicon{fa-meanpath}{fa-meanpath}{F20C}{4.2}
\showcaseicon{fa-buysellads}{fa-buysellads}{F20D}{4.3}
\showcaseicon{fa-connectdevelop}{fa-connectdevelop}{F20E}{4.3}
\showcaseicon{fa-connectdevelop}{connectdevelop}{F20E}{4.3}
\showcaseicon{fa-dashcube}{fa-dashcube}{F210}{4.3}
\showcaseicon{fa-forumbee}{fa-forumbee}{F211}{4.3}
\showcaseicon{fa-leanpub}{fa-leanpub}{F212}{4.3}
\showcaseicon{fa-sellsy}{fa-sellsy}{F213}{4.3}
\showcaseicon{fa-shirtsinbulk}{fa-shirtsinbulk}{F214}{4.3}
\showcaseicon{fa-simplybuilt}{fa-simplybuilt}{F215}{4.3}
\showcaseicon{fa-skyatlas}{fa-skyatlas}{F216}{4.3}
\showcaseicon{fa-cart-plus}{fa-cart-plus}{F217}{4.3}
\showcaseicon{fa-cart-arrow-down}{fa-cart-arrow-down}{F218}{4.3}
\showcaseicon{fa-diamond}{fa-diamond}{F219}{4.3}
\showcaseicon{fa-ship}{fa-ship}{F21A}{4.3}
\showcaseicon{fa-user-secret}{fa-user-secret}{F21B}{4.3}
\showcaseicon{fa-motorcycle}{fa-motorcycle}{F21C}{4.3}
\showcaseicon{fa-street-view}{fa-street-view}{F21D}{4.3}
\showcaseicon{heartbeat}{faHeartbeat}{F21E}{4.3}
\showcaseicon{fa-venus}{fa-venus}{F221}{4.3}
\showcaseicon{fa-mars}{fa-mars}{F222}{4.3}
\showcaseicon{fa-mercury}{fa-mercury}{F223}{4.3}
\showcaseicon{fa-transgender}{fa-transgender}{F224}{4.3}
\showcaseicon{fa-transgender-alt}{fa-transgender-alt}{F225}{4.3}
\showcaseicon{fa-venus-double}{fa-venus-double}{F226}{4.3}
\showcaseicon{fa-mars-double}{fa-mars-double}{F227}{4.3}
\showcaseicon{fa-venus-mars}{fa-venus-mars}{F228}{4.3}
\showcaseicon{fa-mars-stroke}{fa-mars-stroke}{F229}{4.3}
\showcaseicon{fa-mars-stroke-v}{fa-mars-stroke-v}{F22A}{4.3}
\showcaseicon{fa-mars-stroke-h}{fa-mars-stroke-h}{F22B}{4.3}
\showcaseicon{fa-neuter}{fa-neuter}{F22C}{4.3}
\showcaseicon{fa-facebook-official}{fa-facebook-official}{F230}{4.3}
\showcaseicon{fa-pinterest-p}{fa-pinterest-p}{F231}{4.3}
\showcaseicon{fa-whatsapp}{fa-whatsapp}{F232}{4.3}
\showcaseicon{fa-server}{fa-server}{F233}{4.3}
\showcaseicon{fa-user-plus}{fa-user-plus}{F234}{4.3}
\showcaseicon{fa-user-times}{fa-user-times}{F235}{4.3}
\showcaseicon{fa-bed}{faBed}{F236}{4.3}
\showcaseicon{fa-hotel}{fa-hotel}{F236}{4.3}
\showcaseicon{fa-bed}{fa-bed}{F236}{4.3}
\showcaseicon{fa-viacoin}{fa-viacoin}{F237}{4.3}
\showcaseicon{fa-train}{fa-train}{F238}{4.3}
\showcaseicon{fa-subway}{fa-subway}{F239}{4.3}
\showcaseicon{fa-medium}{fa-medium}{F23A}{4.3}
  \end{showcase}
 
 \iffalse

\subsection{40 New Icons in 4.3\label{section:40_new_icons_in_4.3}}
\begin{showcase}\end{showcase}

\subsection{Web Application Icons\label{section:web_application_icons}}
\begin{showcase}\end{showcase}

\subsection{Transportation Icons\label{section:transportation_icons}}
\begin{showcase}\end{showcase}

\subsection{Gender Icons\label{section:gender_icons}}
\begin{showcase}\end{showcase}

\subsection{File Type Icons\label{section:file_type_icons}}
\begin{showcase}\end{showcase}

\subsection{Spinner Icons\label{section:spinner_icons}}
\begin{showcase}\end{showcase}

\subsection{Form Control Icons\label{section:form_control_icons}}
\begin{showcase}\end{showcase}

\subsection{Payment Icons\label{section:payment_icons}}
\begin{showcase}\end{showcase}

\subsection{Chart Icons\label{section:chart_icons}}
\begin{showcase}\end{showcase}

\subsection{Currency Icons\label{section:currency_icons}}
\begin{showcase}\end{showcase}

\subsection{Text Editor Icons\label{section:text_editor_icons}}
\begin{showcase}\end{showcase}

\subsection{Directional Icons\label{section:directional_icons}}
\begin{showcase}\end{showcase}

\subsection{Video Player Icons\label{section:video_player_icons}}
\begin{showcase}\end{showcase}

\subsection{Brand Icons\label{section:brand_icons}}
\begin{showcase}\end{showcase}

\subsection{Medical Icons\label{section:medical_icons}}
\begin{showcase}\end{showcase}
\fi

\PrintChanges
\PrintIndex
\end{document}


%% end of file `fontawesome.tex'.
